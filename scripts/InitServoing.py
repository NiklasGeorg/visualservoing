#!/usr/bin/env python
## Visual Servoing - Aufruf mit rosrun VisualServoing InitServoing.py [robotino_name No. 1] [robotino_name No. 2]

import rospy
from std_msgs.msg import String
from marker_detect.msg import rosMarker
from marker_detect.msg import rosMarkers
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Twist
from std_msgs.msg import * 
import subprocess
import numpy
import math
import sys
import signal


def callback1(data):
    callback(data, robotino1)

def callback2(data):
    callback(data, robotino2)

def callback(data, robotino):
     # Example: Apollon=Robotino1 ; Boreas=Robotino2
     global state
     if (state==1 and robotino==robotino1):
       # Apollon Rotates until he sees "enough of Boreas" (Both markers on the front or one marker in the middle of the camera frame)
       if (len(data.markers) == 0):
	 rotate(robotino, 0.5)
       else:
	 k=data.markers[0].pose.position.x/data.markers[0].pose.position.z
         if (k<0.19 and k>0.17 or len(data.markers)==2):
           state=2
         else:
	   rotate(robotino, 0.3)
     elif (state==2 and robotino==robotino1):
        # If they are to near to each other to see each other good  - Apollon drives backwards until he has a differnence of 50 (due to camera solution z=50*2=100)
        if (data.markers[0].pose.position.z<1000):
           drive_back(robotino, 0.1)
        else:
	   if (data.markers[0].id==57 or data.markers[0].id==24):
	     state=3
	   else: 
	     state=4
     elif (state==3 and robotino==robotino1):
        # Boreas rotates in chosen (shorter) direction until he sees Apollon
           rotate(robotino2, 0.35)
     elif (state==3 and robotino==robotino2):
       # Boreas rotates in chosen (shorter) direction until he sees Apollon good enough (same criteria as above)
        if (len(data.markers)>0):
           k=data.markers[0].pose.position.x/data.markers[0].pose.position.z
           print(k)
           if (k<0.19 and k>0.17 or len(data.markers)==2):  
              state=6
     elif (state==4 and robotino==robotino1):
       # Boreas rotates in chosen (shorter) direction until he sees Apollon
           rotate(robotino2, -0.35)
     elif (state==4 and robotino==robotino2):
       # Boreas rotates in chosen (shorter) direction until he sees Apollon good enough (same criteria as above)
        if (len(data.markers)>0):
           k=data.markers[0].pose.position.x/data.markers[0].pose.position.z
           if (k<0.19 and k>0.17 or len(data.markers)==2):  
              state=6
     # State 6  -- Finished Initalisation - Start Visual Servoing
         
       
       
def rotate(robotino, speed):
     # rotate robotino with given speed
     vel=Twist()
     vel.angular.z=speed
     pub=rospy.Publisher("/" + str(robotino) + '/cmd_vel', Twist)
     pub.publish(vel)
     
def drive_back(robotino, speed): 
     # drive robotino with given speed backwards
     vel=Twist()
     vel.linear.x=-speed
     pub=rospy.Publisher("/" + str(robotino) + '/cmd_vel', Twist)
     pub.publish(vel)
   
 
def InitServoing():
    rospy.init_node('InitServoing')
    # Subscribing to both topics published by the Robotinos
    rospy.Subscriber("/" + str(robotino1) + "/markers/poses", rosMarkers, callback1)
    rospy.Subscriber("/" + str(robotino2) + "/markers/poses", rosMarkers, callback2)
    # State 6 means that the good starting position is archieved
    while(state!=6 and not rospy.is_shutdown()):
      pass

def exit_handler(sig,frame):
    global run
    run = False
    print("Exiting\n")
        
if __name__ == '__main__':
    run = True
    # Robotino Namespaces einlesen:
    robotino1=sys.argv[1]
    robotino2=sys.argv[2]
    # Zustandsvariable: States werden oben erklaert
    state=1 
    InitServoing()
    # Nach erreichter Position in der sich Roboter gut sehen wird auf beiden Robotinos parallel VisualServoing gestartet
    p1 = subprocess.Popen(["rosrun", "VisualServoing", "VisualServoing.py", str(robotino1)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p2 = subprocess.Popen(["rosrun", "VisualServoing", "VisualServoing.py", str(robotino2)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    signal.signal(signal.SIGINT, exit_handler)
    while run:
        signal.pause()
    p1.terminate()
    p2.terminate()
