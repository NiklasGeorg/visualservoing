#!/usr/bin/env python
## Visual Servoing - Aufruf mit rosrun VisualServoing VisualServoing.py [robotino_name]

import rospy
from std_msgs.msg import String
from marker_detect.msg import rosMarker
from marker_detect.msg import rosMarkers
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Twist
from std_msgs.msg import * 
import numpy
import math
import sys


def callback(data):
    
    pub=rospy.Publisher("/" + str(robotino) + '/cmd_vel', Twist)
    
    current=rosMarker()
    current.id=0
    #Choose Marker
    for i in range(0, len(data.markers)):
      for c in range(0, len(ziele)):
        if (data.markers[i].id==ziele[c].id):
          current=data.markers[i]
          ziel=ziele[c]
    
    if (current.id==0):
      raise Exception("No target marker detected!")
    
    eyehand=numpy.matrix([[1, -0.00261, -0.00021, -7.78874], [-0.00262, -0.99540, -0.09572, 122.01744], [0.00004, 0.09572, -0.99541, -79.11667], [0, 0, 0, 1]])
    #eyehandcalibration was done with different coordinate system so x-axis and y-axis have to be swapped
    eyehand=numpy.matrix([[0, 1, 0, 0], [1, 0, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])*eyehand

    # Just in our case needed because of problems with lower webcam resolution in Servoing part than in calibration part
    eyehand[0:2, 3] = 2*eyehand[0:2, 3]
    
    #estimated eyehand matrix for debugging: 
    #eyehand=numpy.matrix([[0, -1, 0, 0], [1, 0, 0, 0], [0, 0, -1, 0],[0,0,0,1]])
    
    CameraZuMarkerInitial=numpy.matrix([[1, 0, 0, ziel.pose.position.x], [0, 1, 0, ziel.pose.position.y], [0, 0, 1, ziel.pose.position.z], [0, 0, 0, 1]]);
    RoboterZuMarkerInitial= numpy.linalg.inv(eyehand)*numpy.linalg.inv(CameraZuMarkerInitial)

    #current.matrix is a list needs to be reshaped
    TransMat=numpy.reshape(current.matrix, (4, 4))
    #compute orientation angle (Pitch - Angle is the relevant angle because the marker is mounted perpendicular on the robot with y-Axis in vertical diretion
    winkel=math.atan2(-TransMat[2, 0],math.sqrt(TransMat[0, 0]*TransMat[0,0] + TransMat[1, 0]*TransMat[1, 0]))*180/math.pi
    theta_u=(winkel-WinkelInitial) * numpy.matrix([[0], [0], [1]])

    #use eyehand calibration to transform the measured coordinates in the robotino coordinate frame
    CameraZuMarker=numpy.matrix([[1, 0, 0, current.pose.position.x], [0, 1, 0, current.pose.position.y], [0, 0, 1, current.pose.position.z], [0, 0, 0, 1]]);
    RoboterZuMarker=numpy.linalg.inv(eyehand)*numpy.linalg.inv(CameraZuMarker)
    
    #Constant to scale driving speed
    lambd=0.0000001
    
    #Extract distance vectors of current camera frame and target camera frame
    tc=RoboterZuMarker[0:3, 3]
    tcstern=RoboterZuMarkerInitial[0:3, 3]
    
    # Actual Visual Servoing  
    v=-lambd * ((tcstern - tc) + lambd*numpy.matrix([[0, -tc[2], tc[1]], [tc[2], 0, -tc[0]], [-tc[1], tc[0], 0 ]])*theta_u)
    vel=Twist()
    vel.linear.x=v[0]
    vel.linear.y=v[1]
    vel.linear.z=v[2] # egal
    vel.angular.x=0 # egal
    vel.angular.y=0 # egal
    vel.angular.z=-0.8*lambd * theta_u[2]
    
    global state
    if (numpy.linalg.norm(theta_u) > 25 or numpy.linalg.norm(tcstern-tc)> 50):
       # Target changed position and not justed little variations in pose estimation
       state=1
    
    #treshold for target position arrived
    if not ( numpy.linalg.norm(theta_u) < 6 and numpy.linalg.norm(tcstern-tc)< 30):
       if (state==1):
	  # Publish computed velocity to the robotino
	  pub.publish(vel)
    else: 
       # 'Target arrived State = 2' because of strong variations occuring sometimes in the orientation estimation
       state=2

    
def VisualServoing():

    rospy.init_node('VisualServoing' + str(robotino))
    rospy.Subscriber("/" + str(robotino) + "/markers/poses", rosMarkers, callback)
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()
        
if __name__ == '__main__':
    # read robotinos namespace
    robotino=sys.argv[1]
    # read target pose(s) | possibly target marker position of more than one marker (e.g. 2 marked attached on target)
    f=open("position_" + str(robotino))
    ziele=[]
    notEOF=True
    state=1
    while (notEOF):
      w=f.readline()
      if (len(w)>0):
	ziel=rosMarker()
        ziel.id=int(w)
        ziel.pose.position.x = float(f.readline())
        ziel.pose.position.y = float(f.readline())
        ziel.pose.position.z=float(f.readline())
        WinkelInitial=float(f.readline())
        ziele.append(ziel)
      else:
	notEOF=False
    if (len(ziele)<1):
        raise Exception("No target!")
    f.close()
    VisualServoing()
