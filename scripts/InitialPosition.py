#!/usr/bin/env python
# Initial Position

import rospy
from std_msgs.msg import String
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Twist
from marker_detect.msg import rosMarkers
from marker_detect.msg import rosMarker
import math
import numpy
import sys

def callback(data):
    
    if (len(data.markers)>0):
     file=open("position_"+str(robotino), "w")
     for i in range(0,len(data.markers)):
	     file.writelines(str(data.markers[i].id)+"\n");
	     file.writelines(str(data.markers[i].pose.position.x)+"\n")
	     file.writelines(str(data.markers[i].pose.position.y)+"\n")
	     file.writelines(str(data.markers[i].pose.position.z)+"\n")
	     TransMat=numpy.reshape(data.markers[i].matrix, (4, 4))
	     file.writelines(str(math.atan2(-TransMat[2, 0],math.sqrt(TransMat[0, 0]*TransMat[0,0] + TransMat[1, 0]*TransMat[1, 0]))*180/math.pi) + "\n")
             print("Position und Orientierung von Marker mit ID: " + str(data.markers[i].id) + " wurde gespeichert")
     file.close()
     exit()	
     
    
def InitialPosition():

    rospy.init_node('InitialPosition')
    rospy.Subscriber(str(robotino) + "/markers/poses", rosMarkers, callback)
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()
        
if __name__ == '__main__':
  robotino=sys.argv[1]
  InitialPosition()
