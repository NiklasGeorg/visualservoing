#Visual Servoing

This collection of python scripts allows to do visual servoing with robotinos. For Marker recognition it relies on marker detection using the aruco library included into ros with https://bitbucket.org/NiklasGeorg/markerdetection .

## Installation

```
#!bash
cd catkinws
git clone git@bitbucket.org:NiklasGeorg/visualservoing.git
catkin_make
```
And that's it. catkin_make is just for checking and installing the dependencies.

## Usage

### InitialPosition.py
Allows to save one or more markers with their position as the goal position into a file named position_robotino-namespace.
```
#!bash
rosrun VisualServoing InitialPosition.py <robotino-namespace>
```
Then move the Robot to the right position and press Ctl-C.


### VisualServoing.py
Does visual Servoing on a single Robotino. At least one of the Markers has to be visible for the robotino all the time. The target position is read from the file position_robotino-namespace.
```
#!bash
rosrun VisualServoing VisualServoing.py <robotino-namespace>
```

### InitServoing.py 
Makes the two robots see their markers and launches the visual servoing node for each robot.
```
#!bash
rosrun VisualServoing VisualServoing.py <robotino-namespace1> <robotino-namespace2>
```
